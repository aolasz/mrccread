# qcread
Parse data from MRCC and DMRG output files

## Usage
### Set up the python environment
Clone this repository and change to the directory:
```
$ git clone https://gitlab.com/aolasz/qcread.git
$ cd qcread
```
Prepare the python environment (only once):  
(You may need `sudo apt install python3-venv` first in Ubuntu)
```
$ python -m venv .venv
$ source .venv/bin/activate
(Window$> .venv\Scripts\activate.bat)

$ pip install -r requirements.txt
```
Later, it is enough to just activate the environment:
```
$ source .venv/bin/activate
```

### Run the parser 
```
$ ./ringread.py test/*.out
```
or
```
$ ./ringread.py mrccfile1.out mrccfile2.out --xlsxfile ringread.xlsx
```
or
```
$ ./ringread.py mypath/*.out -x results.xlsx
```

Finally deactivate the Python venv:
```
$ deactivate
```
