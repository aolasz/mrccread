#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

"""
MRCC and DMRG out file parser for Be{n} rings
This script allows the user to parse some information of out and cost files and collect
the results in an xlsx file.
This script accepts a list of out files (.out).
The results are written in an xlsx file or to "ringread.xlsx" if no xlsx file is given.
Pre-requisite :
xlsxwriter package to be installed within the Python runtime environment
"""

import argparse
from pathlib import Path
import sys

import xlsxwriter

import utils
from utils import dmrg, mrcc

parser = argparse.ArgumentParser()
parser.add_argument(
    "outfiles",
    nargs="+",
    type=argparse.FileType("rt"),
    default=sys.stdin,
    help="Path to one or more MRCC or DMRG output files",
)
parser.add_argument(
    "-x",
    "--xlsxfile",
    nargs="?",
    type=argparse.FileType("wb"),
    default=open("./ringread.xlsx", "wb"),
    help="Path to result xlsx file",
)
args = parser.parse_args()
fpaths = [Path(outfile.name).resolve() for outfile in args.outfiles]

columns = {
    "Ring": [""],
    "Distance": ["Ångström"],
    "Method": [""],
    "Basis": [""],
    "CPU time": ["[s]"],
    "Memory": ["[MiB]"],
    "Disk write": ["[MiB]"],
    "Disk read": ["[MiB]"],
    "Energy": ["[A.U.]"],
    "Determinants": [""],
    "Wall time": ["[s]"],
}


for outfile in args.outfiles:
    fpath = Path(outfile.name).resolve()
    out_text = outfile.read()
    outfile.close()
    fname_parts = fpath.stem.split("_")
    costfile_name = fpath.with_suffix(".cost")
    if fname_parts[-1] == "Eexpt":
        costfile_name = fpath.with_name(f"{fpath.name.rsplit('_', 1)[0]}.cost")
    with open(costfile_name, "rt") as costfile:
        cost_text = costfile.read()
    for column, value in zip(tuple(columns.keys())[:4], fname_parts[:4]):
        if column == "Distance":
            columns[column].append(float((value.rstrip("A"))))
            continue
        columns[column].append(value.upper())
    costs = utils.read_cost(text=cost_text)

    columns["Wall time"].append(utils.seconds(hms=costs[0]["CPUTime"]))
    columns["CPU time"].append(utils.seconds(hms=costs[1]["CPUTime"]))
    columns["Memory"].append(utils.get_mebi(string=costs[1]["MaxRSS"]))
    columns["Disk write"].append(utils.get_mebi(string=costs[1]["MaxDiskWrite"]))
    columns["Disk read"].append(utils.get_mebi(string=costs[1]["MaxDiskRead"]))
    get_num_determinants = mrcc.get_num_determinants
    get_energy = mrcc.get_energy
    if "DMRG" in columns["Method"][-1].upper():
        get_num_determinants = dmrg.get_num_determinants
        get_energy = dmrg.get_energy
    columns["Determinants"].append(get_num_determinants(text=out_text))
    columns["Energy"].append(get_energy(text=out_text))

    if columns["Determinants"][-1] == "" and columns["Method"][-1].upper() == "CCSD":
        ccsdt_fname_parts = fname_parts[:]
        ccsdt_fname_parts[2] = "ccsdt"
        # ccsdt_fpath = fpath.with_stem("_".join(ccsdt_fname_parts))  # New in Python 3.9
        ccsdt_fpath = fpath.with_name(f"{'_'.join(ccsdt_fname_parts)}.out")
        try:
            with open(ccsdt_fpath, "rt") as f:
                ccsdt_text = f.read()
            columns["Determinants"][-1] = sum(mrcc.get_num_excitations(text=ccsdt_text)[:3])
        except FileNotFoundError:
            pass

workbook = xlsxwriter.Workbook(args.xlsxfile)
worksheet = workbook.add_worksheet()
bold = workbook.add_format({"bold": True})
align_center = workbook.add_format({"align": "center"})
worksheet.set_column("B:B", 10)
worksheet.set_column("J:J", 15)
worksheet.set_row(0, 14.25, bold)
worksheet.set_row(1, 14.25, align_center)

for col_num, (header, column) in enumerate(columns.items()):
    worksheet.write(0, col_num, header)
    worksheet.write_column(1, col_num, column)

workbook.close()
