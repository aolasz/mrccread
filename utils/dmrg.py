# vim: set fileencoding=utf-8 :

"""
DMRG out file parser utility functions
note: visit regex101.com to easily understand and develop regex strings
"""

import functools
import re


def silent(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except KeyError:
            return ""

    return wrapper


@silent
def find(*, kw: str, text: str) -> str:
    """
    Find the value of a keyword in a DMRG output text

    :param kw: the keyword whose value we are looking for
    :param text: text of the DMRG out file
    :returns: the text value of keyword found first
    :raises KeyError: if the keyword is not found
    """
    search = re.search(
        rf"^%?\s?{re.escape(kw)}\s+=\s+([^#\n\r]+);?",
        text,
        flags=re.IGNORECASE | re.MULTILINE,
    )
    if not search:
        raise KeyError(kw)
    return search.group(1)


@silent
def get_energy(*, text: str) -> float:
    """
    Find the last energy value in a DMRG output text

    :param text: text of the DMRG out file
    :returns: the float value of the last "Total energy"
    """
    lines = text.splitlines()
    return float(lines[-1].split()[1])


@silent
def get_num_determinants(*, text: str) -> int:
    """
    Find the total number of determinants as max value of the Dim_? columns

    :param text: text of the DMRG out file
    :returns: the integer value of the total number of determinants
    :raises KeyError: if no line with "A_in = [..." is found
    """
    search = re.findall(
        r"^\s?A_in\s?=\s?\[...\s*\n((?s).*)",
        text,
        flags=re.MULTILINE,
    )
    if len(search) < 1:
        raise KeyError("A_in = [...")

    lines = search[-1].split("\n")
    max_num_det = 0
    for line in lines:
        num_determinants = [int(value) for value in line.split()[9:12]]
        max_num_det = max([max_num_det] + num_determinants)
    return max_num_det
