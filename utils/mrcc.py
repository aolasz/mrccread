# vim: set fileencoding=utf-8 :

"""
MRCC out file parser utility functions
"""

import functools
import re
from typing import List  # Not needed since Python 3.9


def silent(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except KeyError as err:
            return ""

    return wrapper


@silent
def find(*, kw: str, text: str) -> str:
    """
    Find the value of a keyword in an MRCC output text

    :param kw: the keyword whose value we are looking for
    :param text: text of the MRCC out file
    :returns: the text value of keyword found first
    :raises KeyError: if the keyword is not found
    """
    search = re.search(
        rf"^\s?{re.escape(kw)}\s?=\s?([^#\n\r\s]+)",
        text,
        flags=re.IGNORECASE | re.MULTILINE,
    )
    if not search:
        raise KeyError(kw)
    return search.group(1)


@silent
def get_energy(*, text: str) -> float:
    """
    Find the last "Total {calc} energy" value in an MRCC output text,
    where calc is the value of the calc keyword

    :param text: text of the MRCC out file
    :returns: the float value of the last "Total energy"
    :raises KeyError: if no "Total energy" is found
    """
    calc = find(kw="calc", text=text)
    search = re.findall(
        rf"^\s?Total\s{re.escape(calc)}\senergy\s\[au\]:\s+(-?\d+\.\d*)",
        text,
        flags=re.IGNORECASE | re.MULTILINE,
    )
    if len(search) < 1:
        raise KeyError("Total energy")
    return float(search[-1])


@silent
def get_num_determinants(*, text: str) -> int:
    """
    Find the last "Total number of determinants" in an MRCC output text

    :param text: text of the MRCC out file
    :returns: the integer value of the last "Total number of determinants"
    :raises KeyError: if no "Total number of determinants" is found
    """
    search = re.findall(
        r"^\s?Total number of determinants:\s+(\d+)",
        text,
        flags=re.IGNORECASE | re.MULTILINE,
    )
    if len(search) < 1:
        raise KeyError("Total number of determinants")
    return int(search[-1])


@silent
# def get_num_excitations(*, text: str) -> list[int]:  # New in Python 3.9
def get_num_excitations(*, text: str) -> List[int]:
    """
    Find the n-fold excitations in the MRCC output text

    :param text: text of the DMRG out file
    :returns: the list of n-fold excitations from 0 to n
    :raises KeyError: if no line with "Number of {n}-fold excitations" is found
    """
    search = re.findall(
        r"^\s?Number\sof\s\d+-fold\sexcitations:\s(\d+)",
        text,
        flags=re.MULTILINE,
    )
    if len(search) < 1:
        raise KeyError("Number of {n}-fold excitations:")
    return [int(n) for n in search]
